import XCTest
@testable import InfiniteLayout

final class InfiniteLayoutTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(InfiniteLayout().text, "Hello, World!")
    }
}
